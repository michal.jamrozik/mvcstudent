package com.michal;


import com.michal.controller.StudentController;
import com.michal.view.*;

public class Main {
    public static void main(String[] args) {
        StudentMVC.Controller controller= new StudentController();
        Printer printer = new PrinterImpl();
        CustomScanner customScanner = new CustomScannerImpl();
        StudentView view = new StudentView(customScanner,printer);
        view.setController(controller);
        view.runApp();
    }
}
