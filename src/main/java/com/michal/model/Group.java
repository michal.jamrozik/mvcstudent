package com.michal.model;

import java.util.ArrayList;
import java.util.List;

public class Group {
    private String name;
    private List<Student> members = new ArrayList<>();

    public Group(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Student> getMembers() {
        return members;
    }

    public void setMembers(List<Student> members) {
        this.members = members;
    }

    public void addMember(Student student) {
        members.add(student);
    }
}
