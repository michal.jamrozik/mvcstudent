package com.michal;

import com.michal.model.Address;
import com.michal.model.Group;
import com.michal.model.Student;

public interface StudentMVC {

    interface Controller {

        double calculateAvgAge();

        void setGroup(Group group);

        void addStudent(Student student);
    }

    interface View {
        void runApp();

        Address createAddress();

        Student createNewStudent();

        Group createNewGroup();

        int getOption();

        void showMenu();

        void sayGoodbye();

        void printGroupAverage();

        void printStudentDetails(Student student);
    }
}
