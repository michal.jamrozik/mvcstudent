package com.michal.controller;

import com.michal.model.Group;

import com.michal.StudentMVC;
import com.michal.model.Student;

public class StudentController implements StudentMVC.Controller {
    private Group group;

    @Override
    public double calculateAvgAge() {
        return group.getMembers().stream()
                .map(student -> student.getAge())
                .mapToInt(age -> age)
                .average()
                .getAsDouble();
    }

    @Override
    public void setGroup(Group group) {
        this.group = group;
    }

    @Override
    public void addStudent(Student student) {
        group.addMember(student);
    }
}
