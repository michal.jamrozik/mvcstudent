package com.michal.view;

public interface CustomScanner {
    String getNextString();

    int getNextInt();
}
