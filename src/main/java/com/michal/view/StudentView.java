package com.michal.view;

import com.michal.StudentMVC;
import com.michal.model.Address;
import com.michal.model.Group;
import com.michal.model.Student;

import java.text.DecimalFormat;

public class StudentView implements StudentMVC.View {
    private CustomScanner customScanner;
    private Printer printer;
    private StudentMVC.Controller controller;

    public StudentView(CustomScanner customScanner, Printer printer) {
        this.customScanner = customScanner;
        this.printer = printer;
    }

    public void setController(StudentMVC.Controller controller) {
        this.controller = controller;
    }

    @Override
    public void runApp() {
        controller.setGroup(createNewGroup());
        int option = 0;
        boolean flag = true;
        while (flag) {
            showMenu();
            option = getOption();
            if (option == 1) {
                Student student = createNewStudent();
                controller.addStudent(student);
                printStudentDetails(student);
            } else if (option == 2) {
                printGroupAverage();
            } else {
                sayGoodbye();
                flag = false;
            }
        }
    }

    @Override
    public Group createNewGroup() {
        printer.printMsg("Please provide group name: ");
        String name = customScanner.getNextString();
        return new Group(name);
    }

    @Override
    public Address createAddress() {
        printer.printMsg("Please provide city: ");
        String city = customScanner.getNextString();
        printer.printMsg("Please provide street: ");
        String street = customScanner.getNextString();
        return new Address(city, street);
    }

    @Override
    public Student createNewStudent() {
        printer.printMsg("Please provide student name: ");
        String name = customScanner.getNextString();
        printer.printMsg("Please provide student age: ");
        int age = customScanner.getNextInt();
        Address address = createAddress();
        return new Student(name, age, address);
    }

    @Override
    public int getOption() {
        return customScanner.getNextInt();
    }

    @Override
    public void printGroupAverage() {
        DecimalFormat df = new DecimalFormat("#.##");
        printer.printMsg("Average age of group is: " + df.format(controller.calculateAvgAge()));
    }

    @Override
    public void printStudentDetails(Student student) {
        printer.printMsg("New student added!");
        printer.printStudentDetails(student);
    }

    @Override
    public void showMenu() {
        printer.printMsg("Press: 1-add student to group, 2-calculate average of age, other number - exit");
    }

    @Override
    public void sayGoodbye() {
        printer.printMsg("Good bye!!!");
    }
}
