package com.michal.view;

import com.michal.model.Student;

public interface Printer {
    void printMsg(String message);
    void printStudentDetails(Student student);
}
