package com.michal.view;

import java.util.Scanner;

public class CustomScannerImpl implements CustomScanner {
    private Scanner scanner = new Scanner(System.in);

    public String getNextString() {
        return scanner.next();
    }

    public int getNextInt() {
        return scanner.nextInt();
    }
}
