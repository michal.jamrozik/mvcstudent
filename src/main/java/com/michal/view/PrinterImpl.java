package com.michal.view;

import com.michal.model.Student;

public class PrinterImpl implements Printer {
    @Override
    public void printMsg(String message) {
        System.out.println(message);
    }

    @Override
    public void printStudentDetails(Student student) {
        System.out.println(student);
    }

}
