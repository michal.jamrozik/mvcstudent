package com.michal.controller;

import com.michal.model.Address;
import com.michal.model.Group;
import com.michal.model.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class StudentControllerTest {

    private StudentController tested = new StudentController();
    private Address address = new Address("Krakow", "street");
    private Student student = new Student("john", 20, address);
    private Group group = new Group("group");

    @Test
    void should_return_average_age_20() {
        group.addMember(student);
        tested.setGroup(group);
        Assertions.assertEquals(20,tested.calculateAvgAge());
    }





}