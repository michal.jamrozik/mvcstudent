package com.michal.view;

import com.michal.StudentMVC;
import com.michal.model.Address;
import com.michal.model.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class StudentViewTest {
    private Printer printerMock = Mockito.mock(Printer.class);
    private CustomScanner customScannerMock= Mockito.mock(CustomScanner.class);
    private StudentView tested = new StudentView(customScannerMock, printerMock);
    private StudentMVC.Controller controller=Mockito.mock(StudentMVC.Controller.class);

    @Test
    @DisplayName("should print 'Please provide group name: '")
    public void should_print_please_msg1 (){
        when(customScannerMock.getNextString()).thenReturn(Mockito.anyString());
        tested.createNewGroup();
        Mockito.verify(printerMock).printMsg("Please provide group name: ");
    }

    @Test
    public void should_create_group_with_name_team(){
        when(customScannerMock.getNextString()).thenReturn("Team");
        Assertions.assertEquals("Team", tested.createNewGroup().getName());
    }

    //i tak dalej inne testy podobnie

    @Test
    public void should_print_group_average(){
        tested.setController(controller);
        when(controller.calculateAvgAge()).thenReturn(20.0);
        tested.printGroupAverage();
        Mockito.verify(printerMock).printMsg(Mockito.anyString());
    }

    @Test
    public void runApp_should_add_new_student_and_print_average_age(){
        when(controller.calculateAvgAge()).thenReturn(30.0);
        Mockito.doReturn("a","a","a","a").when(customScannerMock).getNextString();
        Mockito.doReturn(1,30,2,3).when(customScannerMock).getNextInt();
        tested.setController(controller);
        tested.runApp();
        Mockito.verify(printerMock).printMsg("Average age of group is: 30");
    }

    @Test
    public void runApp_should_say_goodbye(){
        when(customScannerMock.getNextString()).thenReturn("new group").thenReturn("name")
        .thenReturn("city").thenReturn("street");
        when(customScannerMock.getNextInt()).thenReturn(1).thenReturn(20).thenReturn(2).thenReturn(3);
        when(controller.calculateAvgAge()).thenReturn(30.0);
        tested.setController(controller);
        tested.runApp();
        Mockito.verify(printerMock).printMsg("Good bye!!!");
    }
}